package pl.edu.pw.elka.sag.mapreduce.main; 
 
import org.json.simple.JSONObject;
import org.w3c.dom.Element;

import pl.edu.pw.elka.sag.mapreduce.example.JSONEntityReaderExample;
import pl.edu.pw.elka.sag.mapreduce.example.PlainTextEntityReaderExample;
import pl.edu.pw.elka.sag.mapreduce.example.XMLEntityReaderExample;

public class TestMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		JSONEntityReaderExample jere = new JSONEntityReaderExample();
		jere.setFileName("./res/exampleJson.txt");
		while (jere.hasNext())
			System.out.println(jere.getNextEntity());

		XMLEntityReaderExample xere = new XMLEntityReaderExample();
		xere.setFileName("./res/exampleXml.xml");
		while (xere.hasNext()) {
			Element eElement = (Element) xere.getNextEntity();
			System.out.println("Staff id : " + eElement.getAttribute("id"));
			System.out.println("First Name : " + eElement.getElementsByTagName("firstname").item(0).getTextContent());
			System.out.println("Last Name : " + eElement.getElementsByTagName("lastname").item(0).getTextContent());
			System.out.println("Nick Name : " + eElement.getElementsByTagName("nickname").item(0).getTextContent());
			System.out.println("Salary : " + eElement.getElementsByTagName("salary").item(0).getTextContent());
		}
		
		PlainTextEntityReaderExample ptere = new PlainTextEntityReaderExample();
		ptere.setFileName("./res/exampleText.txt");
		while (ptere.hasNext()) {
			System.out.println( ptere.getNextEntity() );
		}
	}
}
