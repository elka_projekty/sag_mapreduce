package pl.edu.pw.elka.sag.mapreduce.example;
 
import java.util.Map;

import org.json.simple.JSONObject;

import java.util.HashMap;

import pl.edu.pw.elka.sag.mapreduce.interfaces.JMapper;

public class JSONMapperExample implements JMapper<JSONObject, String, Integer>{

	@Override
	public Map<String, Integer> map(JSONObject inputData) {		
		HashMap<String, Integer> map = new HashMap<String, Integer>();		
		map.put(inputData.get("key").toString(), Integer.parseInt( inputData.get("value").toString() ));	
		return map;
	}
}
