package pl.edu.pw.elka.sag.mapreduce.example;
 
import pl.edu.pw.elka.sag.mapreduce.interfaces.JEntityReader;

import java.io.FileReader;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class JSONEntityReaderExample implements JEntityReader<JSONObject> {

	private JSONParser parser;
	private Iterator<JSONObject> iterator;

	public JSONEntityReaderExample() {
		parser = null;
		iterator = null;
	}

	public void setFileName(String fileName) {
		try {
			parser = new JSONParser();
			FileReader fr = new FileReader(fileName);
			Object obj = parser.parse(fr);
			JSONArray jsonArray = (JSONArray) obj;
			iterator = jsonArray.iterator();
		} catch (ParseException e) {
			System.out.println("ERROR + " + e.toString() );
		} catch (Exception e) {
			System.out.println("ERROR + " + e.getMessage() );
		}
	}

	public boolean hasNext() {
		return iterator != null && iterator.hasNext();
	}

	public JSONObject getNextEntity() {
		if(this.hasNext())
			return iterator.next();
		return null;		
	}
}