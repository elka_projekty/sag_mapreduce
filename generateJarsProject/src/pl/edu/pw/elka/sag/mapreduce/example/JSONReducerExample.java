package pl.edu.pw.elka.sag.mapreduce.example;
 
import java.util.List;
import java.util.Map;

import pl.edu.pw.elka.sag.mapreduce.interfaces.JReducer;

public class JSONReducerExample  implements JReducer<String, Integer, Integer>{

	@Override
	public Integer reduce(Map<String, List<Integer>> keyValueData) {

		int sum = 0;
		
		for(String key : keyValueData.keySet())
			for( Object val : keyValueData.get(key) ){
				if(!val.equals(""))
					sum += Integer.parseInt(val.toString());
			}
		
		return new Integer(sum);
	}
}
