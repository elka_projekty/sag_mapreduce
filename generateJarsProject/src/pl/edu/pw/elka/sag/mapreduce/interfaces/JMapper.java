package pl.edu.pw.elka.sag.mapreduce.interfaces;

import java.util.Map;

public interface JMapper<InputType, Key, Value> {
    public Map <Key, Value> map(InputType inputData);
}
