package pl.edu.pw.elka.sag.mapreduce.example;
 
import pl.edu.pw.elka.sag.mapreduce.interfaces.JEntityReader;

import java.io.File;
import java.util.Scanner;

public class PlainTextEntityReaderExample implements JEntityReader<String> {

	private Scanner in;

	public PlainTextEntityReaderExample() {
		in = null;
	}

	public void setFileName(String fileName) {
		try {
			File file = new File(fileName);
			in = new Scanner(file);
		} catch (Exception e) {
		}
	}

	public boolean hasNext() {
		return in != null && in.hasNext();
	}

	public String getNextEntity() {
		if(this.hasNext())
			return in.nextLine();
		else 
			return null;
	}
}