package pl.edu.pw.elka.sag.mapreduce.example;
 
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
import pl.edu.pw.elka.sag.mapreduce.interfaces.JEntityReader;

public class XMLEntityReaderExample implements JEntityReader<Element> {

	private NodeList nList;
	private int counter;

	public XMLEntityReaderExample() {
		nList = null;
		counter = 0;
	}

	public void setFileName(String fileName) {
		try {
			File fXmlFile = new File(fileName);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();
			nList = doc.getElementsByTagName("staff");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public boolean hasNext() {
		return nList != null && counter < nList.getLength();
	}

	public Element getNextEntity() {
		if (this.hasNext()) {
			Node nNode = nList.item(counter++);
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) nNode;
				return eElement;
			}
		}
		return null;
	}
}