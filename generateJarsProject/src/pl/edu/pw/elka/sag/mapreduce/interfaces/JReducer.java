package pl.edu.pw.elka.sag.mapreduce.interfaces; 

import java.util.List;
import java.util.Map;

public interface JReducer<Key, Value, OutputType> {
    public OutputType reduce(Map<Key,List<Value>> keyValueData);
}
