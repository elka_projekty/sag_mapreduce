libraryDependencies += "com.googlecode.json-simple" % "json-simple" % "1.1"

libraryDependencies += "com.typesafe.akka" % "akka-actor_2.10" % "2.3.8"

libraryDependencies += "com.typesafe.akka" %% "akka-cluster" % "2.3.8"