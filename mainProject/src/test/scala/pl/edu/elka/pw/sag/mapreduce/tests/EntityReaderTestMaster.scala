package pl.edu.elka.pw.sag.mapreduce.tests

import java.io.File
import java.lang.reflect.Constructor

import akka.actor._
import akka.actor.SupervisorStrategy._
import akka.routing.RoundRobinRouter
import scala.concurrent.duration._
import mapreduce.actors.MapReduceLogger.LogMessageType
import mapreduce.actors.{ CacheActor, MapReduceLogger }
import mapreduce.helpers.MapReduceLogging
import pl.edu.pw.elka.sag.mapreduce.actors.MapReduceMaster.ReduceResult
import pl.edu.pw.elka.sag.mapreduce.interfaces._
import pl.edu.pw.elka.sag.mapreduce.actors._
import pl.edu.elka.pw.sag.mapreduce.tests.actors._

class EntityReaderTestMaster[InputType](
  var entityClassConstructor: Constructor[JEntityReader[InputType]],
  var fileList: List[File]) extends Actor {

  val nrOfFileReaders = 2
  val fileReadersRouter = context.actorOf(
    Props(new FileReaderTestActor[InputType](entityClassConstructor)).withRouter(RoundRobinRouter(nrOfFileReaders)), name = "fileReadersRouter")

  var ansCounter = 0

  def receive = {
    case TestSupervisor.StartEnityTest =>
      for (file <- fileList)
        fileReadersRouter ! Process(file.getAbsolutePath)

    case TestSupervisor.EndOfEntityTest =>
      if ((ansCounter = ansCounter + 1) == fileList.size)
        context.parent ! TestSupervisor.EndOfEntityTest
  }
}