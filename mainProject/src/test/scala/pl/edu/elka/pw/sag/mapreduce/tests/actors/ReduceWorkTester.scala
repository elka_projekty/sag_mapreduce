package pl.edu.elka.pw.sag.mapreduce.tests.actors

import java.lang.reflect.Constructor

import akka.actor._
import akka.event.Logging
import mapreduce.actors.CacheActor.{ MapToShuffleSent, MapToCache }
import mapreduce.actors.MapReduceLogger.LogMessageType
import mapreduce.helpers.MapReduceLogging
import pl.edu.pw.elka.sag.mapreduce.actors.MapWorker.ProcessLine
import pl.edu.pw.elka.sag.mapreduce.actors.ShufflerWorker.MapToShuffle
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import pl.edu.pw.elka.sag.mapreduce.interfaces.JReducer
import pl.edu.elka.pw.sag.mapreduce.tests.ReduceTestMasterObject.ReduceMap
import pl.edu.elka.pw.sag.mapreduce.tests.ReduceTestMasterObject
import scala.collection.mutable.Map

class ReduceWorkTester[Key, Value, OutputType](reducerClassConstructor:Constructor[JReducer[Key, Value, OutputType]]) extends Actor {

  val nrOfReduceWorkers = 10

  val reducerInstance = reducerClassConstructor.newInstance()

  def convetToJavaType(map: scala.collection.mutable.Map[Key, List[Value]]): java.util.Map[Key, java.util.List[Value]] = {
    val withTypes: Map[Key, List[Value]]  = map.asInstanceOf[Map[Key, List[Value]]]
    val withJavaLists: Map[Key, java.util.List[Value]] = withTypes.map(mapElem => {
      val javaList: java.util.List[Value] = mapElem._2
      (mapElem._1, javaList)
    })
    val javaMap: java.util.Map[Key, java.util.List[Value]] =  withJavaLists
    javaMap
  }

  def receive = {    
    case ReduceTestMasterObject.ReduceMap( map ) => 
      val instanedMap:java.util.Map[Key, java.util.List[Value]] = convetToJavaType(map.asInstanceOf[Map[Key,List[Value]]])
      val reduceResult = reducerInstance.reduce(instanedMap)
      println(map.keySet.last + "|" + reduceResult)
  }
}