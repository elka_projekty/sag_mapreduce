package pl.edu.elka.pw.sag.mapreduce.tests

import java.lang.reflect.Constructor

import akka.actor._
import akka.event.Logging
import mapreduce.actors.CacheActor.{ MapToShuffleSent, MapToCache }
import akka.routing.RoundRobinRouter
import mapreduce.actors.MapReduceLogger.LogMessageType
import mapreduce.helpers.MapReduceLogging
import pl.edu.pw.elka.sag.mapreduce.actors.MapWorker.ProcessLine
import pl.edu.pw.elka.sag.mapreduce.actors.ShufflerWorker.MapToShuffle
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import pl.edu.pw.elka.sag.mapreduce.interfaces.JReducer
import pl.edu.elka.pw.sag.mapreduce.tests.actors.ReduceWorkTester
import scala.io.Source
import scala.collection.mutable.Map
import scala.collection.mutable.HashMap
import java.util.LinkedList
import scala.collection.mutable.ListBuffer

object ReduceTestMasterObject {
  case class ReduceMap[Key, Value](var map: HashMap[Key, ListBuffer[Value]])
}

class ReduceTestMaster[Key, Value, OutputType](var reduceClassConstructor: Constructor[JReducer[Key, Value, OutputType]]) extends Actor {

  val nrOfReduceWorkers = 10

  val reducersRouter = context.actorOf(
    Props(new ReduceWorkTester[Key, Value, OutputType](reduceClassConstructor)).withRouter(RoundRobinRouter(nrOfReduceWorkers)), name = "reduceWorkersRouter")

  var keyValueMap: HashMap[Key, ListBuffer[Value]] = new HashMap[Key, ListBuffer[Value]]
  val lines = Source.fromFile("resources/input/keyvalues/testMap.csv").getLines()
  for (line <- lines) {
    val keyValue = line.split(";")
    var list: ListBuffer[Value] = new ListBuffer[Value]()
    var i = 0
    for (i <- 1 to keyValue.length - 1)
      list.add(keyValue(i).asInstanceOf[Value])
    keyValueMap.put(keyValue(0).asInstanceOf[Key], list)
  }

  def receive = {
    case TestSupervisor.StartReduceTest =>
      for (key <- keyValueMap.keySet) {
        var tmp: HashMap[Key, ListBuffer[Value]] = new HashMap[Key, ListBuffer[Value]]
        tmp.put(key, keyValueMap.get(key).get)
        reducersRouter ! ReduceTestMasterObject.ReduceMap[Key, Value](tmp)
      }
      sender ! TestSupervisor.EndOfReduceTest
  }
}