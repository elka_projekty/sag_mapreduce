package pl.edu.elka.pw.sag.mapreduce.tests.actors

import java.lang.reflect.Constructor

import akka.actor._
import akka.event.Logging
import mapreduce.actors.CacheActor.{MapToShuffleSent, MapToCache}
import mapreduce.actors.MapReduceLogger.LogMessageType
import mapreduce.helpers.MapReduceLogging
import pl.edu.pw.elka.sag.mapreduce.actors.MapWorker.ProcessLine
import pl.edu.pw.elka.sag.mapreduce.actors.ShufflerWorker.MapToShuffle
import pl.edu.pw.elka.sag.mapreduce.interfaces.JMapper
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import pl.edu.elka.pw.sag.mapreduce.tests.MapTestMaster.ProcessLine

class MapWorkTester[InputType, Key, Value]( mapClassConstructor: Constructor[JMapper[InputType, Key, Value]]) extends Actor  {

  val jMapperInstance =  mapClassConstructor.newInstance().asInstanceOf[JMapper[InputType, Key, Value]]
  
  def receive = {
    case pl.edu.elka.pw.sag.mapreduce.tests.MapTestMaster.ProcessLine(line) =>
      val maResult = jMapperInstance.map(line.asInstanceOf[InputType]).asScala.toMap
      for( entry <- maResult.keySet )
        println("KEY: " + entry.toString() + " , VALUE: " + maResult.get(entry))
     case _ => println("ODEBRANO COKOLIWEK")
  }
}