package pl.edu.elka.pw.sag.mapreduce.tests

import java.io.File
import java.lang.reflect.Constructor

import akka.actor._
import akka.actor.SupervisorStrategy._
import akka.routing.RoundRobinRouter
import scala.concurrent.duration._
import mapreduce.actors.MapReduceLogger.LogMessageType
import mapreduce.actors.{ CacheActor, MapReduceLogger }
import mapreduce.helpers.MapReduceLogging
import pl.edu.pw.elka.sag.mapreduce.actors.MapReduceMaster.ReduceResult
import pl.edu.pw.elka.sag.mapreduce.interfaces._
import pl.edu.elka.pw.sag.mapreduce.tests.actors._
import pl.edu.elka.pw.sag.mapreduce.tests._
import org.json.simple.JSONObject
import java.util.Iterator
import org.json.simple.JSONArray
import org.json.simple.parser.JSONParser
import org.json.simple.parser.ParseException
import java.io.FileNotFoundException
import java.io.FileReader
import java.io.IOException

object MapTestMaster {
  case class StartMapTest()
  case class ProcessLine[InputType](line: InputType)
}


class MapTestMaster[InputType, Key, Value](
  var mapClassConstructor: Constructor[JMapper[InputType, Key, Value]],
  var fileList: List[File]) extends Actor {

  val nrOfMapWorkers = 2

  val mapWorkersRouter = context.actorOf(
    Props(new MapWorkTester[InputType, Key, Value](mapClassConstructor)).withRouter(RoundRobinRouter(nrOfMapWorkers)), name = "mapWorkersRouter")

  var parser = new JSONParser();
  def receive = {
    case TestSupervisor.StartMapTest =>
      try {
        for (file <- fileList) {
          var obj = parser.parse(new FileReader(file.getAbsolutePath))
          var jsonObject = obj.asInstanceOf[JSONArray]
          var iterator = jsonObject.iterator()
          while (iterator.hasNext()) {
            var n = iterator.next()
            mapWorkersRouter ! MapTestMaster.ProcessLine(n)
          }
        }
        sender ! TestSupervisor.EndOfMapTest
      } catch {
        case e: Exception => println(e.toString())
      }
  }
}