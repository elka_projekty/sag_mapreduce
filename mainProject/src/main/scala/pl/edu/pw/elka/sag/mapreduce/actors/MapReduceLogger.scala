package mapreduce.actors

import akka.actor.Actor
import akka.actor.Actor.Receive
import akka.event.Logging
import mapreduce.actors.MapReduceLogger.LogMessage
import mapreduce.actors.MapReduceLogger.LogMessageType.LogMessageType

object MapReduceLogger {
  object LogMessageType extends Enumeration {
    type LogMessageType = Value
    val GotMessage, CalculactionFinished, SentMessage = Value
  }
  case class LogMessage(from: String, whatHappened: String, logMessageType: LogMessageType)
}

class MapReduceLogger extends Actor{

  val log = Logging(context.system, this)

  def log(from: String, whatHappened: String, messageType: LogMessageType): Unit = {
    log.info(s"[ $messageType ] From: $from. What happened: $whatHappened")
  }

  override def receive: Receive = {
    case  LogMessage(from, whatHappened, logMessageType) => log(from, whatHappened, logMessageType)
  }
}
