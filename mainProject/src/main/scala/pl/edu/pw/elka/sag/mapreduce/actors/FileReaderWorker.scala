package pl.edu.pw.elka.sag.mapreduce.actors

import java.lang.reflect.Constructor

import akka.actor._
import akka.event.Logging
import akka.routing.RoundRobinRouter
import mapreduce.actors.MapReduceLogger
import mapreduce.actors.MapReduceLogger.LogMessageType
import mapreduce.helpers.MapReduceLogging
import pl.edu.pw.elka.sag.mapreduce.actors.MapWorker.ProcessLine
import pl.edu.pw.elka.sag.mapreduce.interfaces.JEntityReader
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._

//agent do czytania i plikow i przesyalania ich do workerow

class FileReaderWorker[InputType](var loggerActor: ActorRef, val mapWorkersRouter: ActorRef, entityClassConstructor: Constructor[JEntityReader[InputType]]) extends Actor with MapReduceLogging {
  
  val jEntityReaderInstance =  entityClassConstructor.newInstance().asInstanceOf[JEntityReader[InputType]]
  
  def receive = {
    case FileName(file) => {      
      jEntityReaderInstance.setFileName(file)
      while(jEntityReaderInstance.hasNext())
        mapWorkersRouter ! ProcessLine[InputType](jEntityReaderInstance.getNextEntity)
    }
  }
}