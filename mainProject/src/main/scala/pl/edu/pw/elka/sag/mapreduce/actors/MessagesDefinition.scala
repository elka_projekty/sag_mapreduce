package pl.edu.pw.elka.sag.mapreduce.actors

import java.io.File

case class Work(start: Int, nrOfElements: Int)
case class Result(value: Int)
case object Calculate 
case class FileName(file : String)
case class TotalResult(value: Int)