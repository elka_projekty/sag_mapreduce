package pl.edu.pw.elka.sag.mapreduce.actors

import java.lang.reflect.Constructor

import akka.actor.{RootActorPath, ActorRef, Actor}
import akka.cluster.{MemberStatus, Member, Cluster}
import akka.cluster.ClusterEvent.{CurrentClusterState, MemberUp}
import mapreduce.actors.MapReduceLogger.LogMessageType
import mapreduce.helpers.MapReduceLogging
import pl.edu.pw.elka.sag.mapreduce.actors.MapReduceMaster.{ReducerRegistration, ReduceResult}
import pl.edu.pw.elka.sag.mapreduce.actors.ReduceWorker.{SystemActorRefs, MapToReduce}
import pl.edu.pw.elka.sag.mapreduce.interfaces.JReducer
import scala.collection.Map
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._


object ReduceWorker {
  case class MapToReduce[Key, Value](map: Map[Key, List[Value]])
  case class SystemActorRefs(masterRef:ActorRef, logger:ActorRef)
}

class ReduceWorker[Key, Value, OutputType](var loggerActor: ActorRef, reducerClassConstructor:Constructor[JReducer[Key, Value, OutputType]]) extends Actor with MapReduceLogging {
  val reducerInstance = reducerClassConstructor.newInstance()
  var resultsKeeper:ActorRef = self
  val cluster = Cluster(context.system)

  override def preStart(): Unit = cluster.subscribe(self, classOf[MemberUp])
  override def postStop(): Unit = cluster.unsubscribe(self)

  def register(member: Member): Unit =
    if (member.hasRole("frontend"))
      context.actorSelection(RootActorPath(member.address) / "user" / "supervisor") !
        ReducerRegistration

  def convetToJavaType(map: Map[Any, List[Any]]): java.util.Map[Key, java.util.List[Value]] = {
    val withTypes: Map[Key, List[Value]]  = map.asInstanceOf[Map[Key, List[Value]]]
    val withJavaLists: Map[Key, java.util.List[Value]] = withTypes.map(mapElem => {
      val javaList: java.util.List[Value] = mapElem._2.asJava
      (mapElem._1, javaList)
    })
    val javaMap: java.util.Map[Key, java.util.List[Value]] =  withJavaLists
    javaMap
  }

  override def receive: Receive = {
    case SystemActorRefs(m,l) =>
      resultsKeeper = m
      loggerActor = l
    case MapToReduce(map) =>
      log(s"got map to reduce", LogMessageType.GotMessage)
      val instanedMap:java.util.Map[Key, java.util.List[Value]] = convetToJavaType(map)
      val reduceResult: OutputType = reducerInstance.reduce(instanedMap)
      log(s"sent result for key $map to results keeper", LogMessageType.SentMessage)

      resultsKeeper ! ReduceResult(map, reduceResult)
    case state: CurrentClusterState =>
      state.members.filter(_.status == MemberStatus.Up) foreach register
    case MemberUp(m) => register(m)
  }
}
