package pl.edu.pw.elka.sag.mapreduce.config

import java.io.File
import pl.edu.pw.elka.sag.mapreduce.interfaces._

class JobConfiguration(val mapperClassName: String, val reducerClassName: String, val inputDataDirectoryPath: String,
                       val outputDataDirectoryPath: String,
                       val entityReader: String, val jarToLoad: String) {

  private def getListOfFiles(dir: String): List[File] = {
    val d = new File(dir)
    if (d.exists && d.isDirectory) {
      d.listFiles.filter(_.isFile).toList
    } else {
      List[File]()
    }
  }

  def getFilesList(): List[File] = {
    if (inputDataDirectoryPath != null || inputDataDirectoryPath.length() > 0)
      getListOfFiles(inputDataDirectoryPath)
    else
      null
  }

}