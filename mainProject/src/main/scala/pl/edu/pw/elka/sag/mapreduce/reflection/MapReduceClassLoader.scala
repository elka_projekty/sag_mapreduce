package pl.edu.pw.elka.sag.mapreduce.reflection

import java.io.File
import java.lang.reflect.Constructor
import java.net.{ URL, URLClassLoader }

import pl.edu.pw.elka.sag.mapreduce.interfaces._

class MapReduceClassLoader[InputType, Key, Value, OutputType](pathToJarFile: String) {
  val file = new File(pathToJarFile)
  val url: URL = file.toURI.toURL
  val urls = Array(url)
  val cl = new URLClassLoader(urls, this.getClass().getClassLoader())

  def loadJMapperConstructor(packagePathToJMapperImplementation: String): Constructor[JMapper[InputType, Key, Value]] = {
    val clazz: Class[JMapper[InputType, Key, Value]] = Class.forName(packagePathToJMapperImplementation, true, cl).asInstanceOf[Class[JMapper[InputType, Key, Value]]]
    clazz.getConstructor()
  }

  def loadJReducerConstructor(packagePathToJReducerImplementation: String): Constructor[JReducer[Key, Value, OutputType]] = {
    val clazz: Class[JReducer[Key, Value, OutputType]] = Class.forName(packagePathToJReducerImplementation, true, cl).asInstanceOf[Class[JReducer[Key, Value, OutputType]]]
    clazz.getConstructor()
  }

  def loadJEntityReaderConstructor(packagePathToJEntityReaderImplementation: String): Constructor[JEntityReader[InputType]] = {
    val clazz: Class[JEntityReader[InputType]] = Class.forName(packagePathToJEntityReaderImplementation, true, cl).asInstanceOf[Class[JEntityReader[InputType]]]
    clazz.getConstructor()
  }
}
