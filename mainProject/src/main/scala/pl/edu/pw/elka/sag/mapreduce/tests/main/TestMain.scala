package pl.edu.elka.pw.sag.mapreduce.tests.main

import java.io.File
import java.lang.reflect.Constructor
import java.util

import pl.edu.elka.pw.sag.mapreduce.tests._
import pl.edu.pw.elka.sag.mapreduce.config.JobConfiguration
import pl.edu.pw.elka.sag.mapreduce.reflection.MapReduceClassLoader
import pl.edu.pw.elka.sag.mapreduce.interfaces._
import org.w3c.dom.Element
import org.json.simple.JSONObject

import akka.actor._

object TestMain extends App {

  val cl = new MapReduceClassLoader("resources/jars/jartoload.jar")
  val jsonEntityReaderConstructor = cl.loadJEntityReaderConstructor("pl.edu.pw.elka.sag.mapreduce.example.JSONEntityReaderExample").asInstanceOf[Constructor[JEntityReader[JSONObject]]]
  val xmlEntityReaderConstructor = cl.loadJEntityReaderConstructor("pl.edu.pw.elka.sag.mapreduce.example.XMLEntityReaderExample").asInstanceOf[Constructor[JEntityReader[Element]]]
  val plainEntityReaderConstructor = cl.loadJEntityReaderConstructor("pl.edu.pw.elka.sag.mapreduce.example.PlainTextEntityReaderExample").asInstanceOf[Constructor[JEntityReader[String]]]

  val system = ActorSystem("TEST")

  val jsonFileReaderSupervisor = system.actorOf(Props(
    new EntityReaderTestMaster[JSONObject](jsonEntityReaderConstructor, getListOfFiles("resources/input/json"))),
    name = "jsonFileReaderSupervisor")

  val xmlFileReaderSupervisor = system.actorOf(Props(
    new EntityReaderTestMaster[Element](xmlEntityReaderConstructor, getListOfFiles("resources/input/xml"))),
    name = "xmlFileReaderSupervisor")

  val plainFileReaderSupervisor = system.actorOf(Props(
    new EntityReaderTestMaster[String](plainEntityReaderConstructor, getListOfFiles("resources/input/plain"))),
    name = "plainFileReaderSupervisor")

  val jsonMapperConstructor = cl.loadJMapperConstructor("pl.edu.pw.elka.sag.mapreduce.example.JSONMapperExample").asInstanceOf[Constructor[JMapper[JSONObject, String, Int]]]
  val jsonMapperSupervisor = system.actorOf(Props(
    new MapTestMaster[JSONObject, String, Int](jsonMapperConstructor, getListOfFiles("resources/input/json"))),
    name = "jsonMapperSupervisor")

  val jsonReducerConstructor = cl.loadJMapperConstructor("pl.edu.pw.elka.sag.mapreduce.example.JSONReducerExample").asInstanceOf[Constructor[JReducer[String, Int, Int]]]
  val jsonReducerSupervisor = system.actorOf(Props(
    new ReduceTestMaster[String, Int, Int](jsonReducerConstructor)),
    name = "jsonReducerSupervisor")

  println("===================================POCZATEK TESTOW================================")
  println("===================================ODCZYT Z JSON-a================================")
  jsonFileReaderSupervisor ! TestSupervisor.StartEnityTest
  Thread.sleep(1000)
  println("===================================ODCZYT Z XML-a================================")
  xmlFileReaderSupervisor ! TestSupervisor.StartEnityTest
  Thread.sleep(1000)
  println("===================================ODCZYT Z PLIKU PROSTEGO================================")
  plainFileReaderSupervisor ! TestSupervisor.StartEnityTest
  Thread.sleep(1000)
  println("===================================TEST MAPPERA================================")
  jsonMapperSupervisor ! TestSupervisor.StartMapTest
  Thread.sleep(1000)
  println("===================================TEST REDUCERA================================")
  jsonReducerSupervisor ! TestSupervisor.StartReduceTest
  Thread.sleep(1000)
  println("=================================KONIEC TESTOW================================")
  system.shutdown()
  def getListOfFiles(dir: String): List[File] = {
    val d = new File(dir)
    if (d.exists && d.isDirectory) {
      d.listFiles.filter(_.isFile).toList
    } else {
      List[File]()
    }
  }
}

