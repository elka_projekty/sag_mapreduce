package pl.edu.pw.elka.sag.mapreduce.actors

import java.lang.reflect.Constructor

import akka.actor._
import akka.event.Logging
import mapreduce.actors.CacheActor.{MapToShuffleSent, MapToCache}
import mapreduce.actors.MapReduceLogger.LogMessageType
import mapreduce.helpers.MapReduceLogging
import pl.edu.pw.elka.sag.mapreduce.actors.MapWorker.ProcessLine
import pl.edu.pw.elka.sag.mapreduce.actors.ShufflerWorker.MapToShuffle
import pl.edu.pw.elka.sag.mapreduce.interfaces.JMapper
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._

object MapWorker {

  case class ProcessLine[InputType](line: InputType)

}

class MapWorker[InputType, Key, Value](var loggerActor: ActorRef, shufflersRouter: ActorRef, mapClassConstructor: Constructor[JMapper[InputType, Key, Value]], cacheActor:ActorRef) extends Actor with MapReduceLogging {

  val jMapperInstance =  mapClassConstructor.newInstance().asInstanceOf[JMapper[Object, Key, Value]]
  val log = Logging(context.system, this)

  def receive = {
    case ProcessLine(line) =>
      log("got message to process line", LogMessageType.GotMessage)
      val maResult = jMapperInstance.map(line.asInstanceOf[Object]).asScala
      log(s"sending $maResult to shufflersRouter", LogMessageType.SentMessage)
      cacheActor ! MapToShuffleSent
      shufflersRouter ! MapToShuffle[Key, Value](maResult) // perform the work
  }
}