package pl.edu.pw.elka.sag.mapreduce.actors

import java.io.File
import java.lang.reflect.Constructor

import akka.actor.SupervisorStrategy._
import akka.actor._
import akka.routing.{Broadcast, RoundRobinRouter}
import mapreduce.actors.CacheActor.NewReducers
import mapreduce.actors.MapReduceLogger.LogMessageType
import mapreduce.actors.{CacheActor, MapReduceLogger}
import mapreduce.helpers.MapReduceLogging
import pl.edu.pw.elka.sag.mapreduce.actors.MapReduceMaster.{ReducerRegistration, ReduceResult}
import pl.edu.pw.elka.sag.mapreduce.actors.ReduceWorker.SystemActorRefs
import pl.edu.pw.elka.sag.mapreduce.actors.ShufflerWorker.FreeReducer
import pl.edu.pw.elka.sag.mapreduce.interfaces._
import scala.concurrent.ExecutionContext.Implicits.global

import scala.concurrent.duration._
import scala.util.Random

object MapReduceMaster {

  case class ReduceResult(key: Any, reduceResult: Any)
//  case class OneReducerPlease()
  case object ReducerRegistration
}

class MapReduceMaster[InputType, Key, Value, OutputType](var fileList: List[File],
                                                         var mapClassConstructor: Constructor[JMapper[InputType, Key, Value]],
                                                         var reduceClassConstructor: Constructor[JReducer[Key, Value, OutputType]],
                                                         var entityClassConstructor: Constructor[JEntityReader[InputType]]) extends Actor with MapReduceLogging {

  val nrOfMapWorkers = 10
  val nrOfShufflerWorkers = 10
  val nrOfReduceWorkers = 10
  val nrOfFileReaders = 2
  var currentResults = 0
  var totalRes = 0

  var loggerActor = context.actorOf(Props[MapReduceLogger], name = "MapReduceLogger")

  var reducers = IndexedSeq.empty[ActorRef]

  val cacheActor = context.actorOf(Props(new CacheActor[Key, Value](loggerActor)), name = "cacheActor")

  val shufflersRouter = context.actorOf(
    Props(new ShufflerWorker(cacheActor, loggerActor)).withRouter(RoundRobinRouter(nrOfShufflerWorkers)), name = "shufflerWorkersRouter")

  val mapWorkersRouter = context.actorOf(
    Props(new MapWorker[InputType, Key, Value](loggerActor, shufflersRouter, mapClassConstructor, cacheActor)).withRouter(RoundRobinRouter(nrOfMapWorkers)), name = "mapWorkersRouter")

  val fileReadersRouter = context.actorOf(
    Props(new FileReaderWorker[InputType](loggerActor, mapWorkersRouter, entityClassConstructor)).withRouter(RoundRobinRouter(nrOfFileReaders)), name = "fileReadersRouter")

  override val supervisorStrategy =
    OneForOneStrategy(maxNrOfRetries = 10, withinTimeRange = 1 minute) {
      case _: ArithmeticException => Resume
      case _: NullPointerException => Restart
      case _: IllegalArgumentException => Stop
      case _: Exception => Escalate
    }

  def receive = {
    case Calculate =>
      if (! (reducers.size > 1)) {
        log(s"Not enough reducers to start program!", LogMessageType.GotMessage)
        context.system.scheduler.scheduleOnce(5.seconds, self, Calculate)
      } else {
        if (fileList.size == 0)
          throw new Exception("Empty input file list")
        for (file <- fileList)
          fileReadersRouter ! FileName(file.getAbsolutePath)
      }

    case ReducerRegistration if !reducers.contains(sender()) =>
      context watch sender()
      sender ! SystemActorRefs(self, loggerActor)
      reducers = reducers :+ sender()
      shufflersRouter ! Broadcast(FreeReducer(reducers))


    case Terminated(a) =>
      reducers = reducers.filterNot(_ == a)
      shufflersRouter ! FreeReducer(reducers)

    case ReduceResult(key, result) =>
      log(s"got reduce result: $result for key $key", LogMessageType.GotMessage)

//    case OneReducerPlease() => {
//      log(s"got request for receivers", LogMessageType.GotMessage)
//      sender() ! FreeReducer(reducers(Random.nextInt(reducers.length)))
//    }
  }
}