package pl.edu.elka.pw.sag.mapreduce.tests

import java.lang.reflect.Constructor
import java.util

import pl.edu.pw.elka.sag.mapreduce.config.JobConfiguration
import pl.edu.pw.elka.sag.mapreduce.actors._
import akka.actor._
import pl.edu.pw.elka.sag.mapreduce.interfaces._
import pl.edu.pw.elka.sag.mapreduce.reflection.MapReduceClassLoader
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import scala.collection.immutable.Stream.cons

case object StartFileReaderTest
case class Process(val file: String)

class TestJob(val jobConfiguration: JobConfiguration, val jobName: String) {

  def run: Unit = {
    //start master
    val cl = new MapReduceClassLoader(jobConfiguration.jarToLoad)
    //val jMapperConstructor = cl.loadJMapperConstructor(jobConfiguration.entityReader).asInstanceOf[Constructor[JMapper[String, String, String]]]
    //val jReducerConstructor = cl.loadJReducerConstructor("").asInstanceOf[Constructor[JReducer[String, String, String]]]
    val jEntityReaderConstructor = cl.loadJEntityReaderConstructor(jobConfiguration.entityReader).asInstanceOf[Constructor[JEntityReader[String]]]

    val system = ActorSystem(jobName)

    val fileReaderSupervisor = system.actorOf(Props(
      new EntityReaderTestMaster[String](jEntityReaderConstructor, jobConfiguration.getFilesList())),
      name = "EntityReaderTestMaster")

      fileReaderSupervisor ! TestSupervisor.StartEnityTest
      
//    val mapperSupervisor = system.actorOf(Props(
//      new MapTestMaster[String, String, String](jMapperConstructor)),
//      name = "mapperSupervisor")
//
//    val reducerSupervisor = system.actorOf(Props(
//      new ReduceTestMaster[String, String, String](jReducerConstructor)),
//      name = "reducerSupervisor")
  }
}