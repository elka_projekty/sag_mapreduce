package pl.edu.pw.elka.sag.mapreduce.main

import pl.edu.pw.elka.sag.mapreduce.config.JobConfiguration
import pl.edu.pw.elka.sag.mapreduce.job.Job
import pl.edu.pw.elka.sag.mapreduce.reflection.MapReduceClassLoader

object Main extends App {
  //basic use case
  val jobConf = new JobConfiguration("pl.edu.pw.elka.sag.mapreduce.example.JSONMapperExample",
                                     "pl.edu.pw.elka.sag.mapreduce.example.JSONReducerExample",
                                     "resources/input/json",
                                      "resources/output",
                                      "pl.edu.pw.elka.sag.mapreduce.example.JSONEntityReaderExample",
                                      "resources/jars/jartoload.jar")
  val job = new Job[String, String, Int, Int](jobConf, "test")
  job.run
}
