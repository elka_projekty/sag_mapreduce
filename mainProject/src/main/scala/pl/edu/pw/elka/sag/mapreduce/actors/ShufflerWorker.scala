package pl.edu.pw.elka.sag.mapreduce.actors

import akka.actor.{ReceiveTimeout, ActorRef, Actor}
import mapreduce.actors.MapReduceLogger.LogMessageType
import mapreduce.helpers.MapReduceLogging
import pl.edu.pw.elka.sag.mapreduce.actors.ReduceWorker.MapToReduce
import pl.edu.pw.elka.sag.mapreduce.actors.ShufflerWorker._

import scala.collection.mutable.Map
import scala.collection.mutable.MutableList
import scala.collection.mutable.HashMap
import scala.concurrent.duration.Duration
import scala.concurrent.duration.SECONDS
import scala.util.Random

object  ShufflerWorker {
  case class MapToShuffle[Key, Value](map: Map[Key, Value])
  case class KeepValueWithKey[Key, Value](key: Key, value: MutableList[Value])
  case class GiveMeValues[Key](key: Key)
  case class InformAboutNewKey[Key](key: Key)
  case class FreeReducer(reducers: IndexedSeq[ActorRef])
  case class GiveMeBecouseIMGreater[Key](key: Key)
}

class ShufflerWorker[Key, Value](cacheActor: ActorRef, var loggerActor: ActorRef) extends Actor with MapReduceLogging {
  var map : Map[Key, MutableList[Value]] = new HashMap[Key, MutableList[Value]]()
  var reducers = IndexedSeq.empty[ActorRef]

  def shuffle(mapToShuffle: Map[Key, Value]) = {
    mapToShuffle.foreach {
      case (key, value) => {
        log(s"add my key $key with value $value", LogMessageType.GotMessage)
        val list: MutableList[Value] = map.getOrElse(key, MutableList[Value]()) += value
        map.put(key, list)
      }
    }
    printMap(map)
    askTheOthers()
  }

  def askTheOthers() = {
    map.foreach {
      case (key, value) => {
        log(s"ask others for $key", LogMessageType.SentMessage)
        context.actorSelection("../*") ! GiveMeValues(key)
      }
    }
  }

  def sendValueToShuffler(key: Key, sender: ActorRef) = {
    if (sender != self && map.get(key) != None) {
      if(sender.hashCode() < self.hashCode()) {
        log(s"send key $key with value $map.get(key)", LogMessageType.SentMessage)
          sender ! KeepValueWithKey(key, map.getOrElse(key, MutableList[Value]()))
          map.remove(key)
      } else {
        log(s"force to send key $key", LogMessageType.SentMessage)
        sender ! GiveMeBecouseIMGreater(key)
      }
    }
  }

  def addValueWithKey(key: Key, value: MutableList[Value]): Unit ={
    var inform: Boolean = false
    val optionList: Option[MutableList[Value]] = map.get(key)
    var list: MutableList[Value] = MutableList[Value]()
    optionList match {
      case Some(valueList)  => list = valueList
      case None             => inform = true
    }
    value.foreach{
      case (v) => {
        list += v
        log(s"add got key $key with value $v", LogMessageType.GotMessage)
      }
    }
    map.put(key, list)
    printMap(map)
    if (inform)
      context.actorSelection("../*") ! InformAboutNewKey(key)
  }

  def sendListsToReducers() = {
    //val mapToSend: Map[Key, List[Value]] = HashMap[Key, List[Value]]()
    map.foreach{
      case (key, value) => {
        val mapToSend: Map[Key, List[Value]] = HashMap[Key, List[Value]]()
        mapToSend.put(key, value.toList)
        log(s"send map to receivers", LogMessageType.SentMessage)
        printLMap(mapToSend)
        reducers(Random.nextInt(reducers.length)) ! MapToReduce(mapToSend)
      }
    }
//    reduceActor ! MapToReduce(mapToSend)
  }

  def checkIfHaveKey(key: Key, sender: ActorRef) = {
    if (map.contains(key))
      sender ! GiveMeValues(key)
  }

  override def receive: Receive = {
    case MapToShuffle(map)  =>
      context.setReceiveTimeout(Duration(1, SECONDS))
      shuffle(map.asInstanceOf[Map[Key, Value]])
    case GiveMeValues(key)            =>
      context.setReceiveTimeout(Duration(1, SECONDS))
      sendValueToShuffler(key.asInstanceOf[Key], sender())
    case KeepValueWithKey(key, value) =>
      context.setReceiveTimeout(Duration(1, SECONDS))
      addValueWithKey(key.asInstanceOf[Key], value.asInstanceOf[MutableList[Value]])
    case InformAboutNewKey(key)       =>
      context.setReceiveTimeout(Duration(1, SECONDS))
      checkIfHaveKey(key.asInstanceOf[Key], sender())
    case FreeReducer(reducer)         =>
      reducers = reducer
    case GiveMeBecouseIMGreater(key)  =>
      context.setReceiveTimeout(Duration(1, SECONDS))
      sendValueToShuffler(key.asInstanceOf[Key], sender())
    case ReceiveTimeout               =>
      log(s"send maps to receivers", LogMessageType.SentMessage)
      printMap(map)
      context.setReceiveTimeout(Duration.Undefined)
      sendListsToReducers()
//    case ReceiveTimeout               => sendListsToReducers()
  }

  def printMap(result: Map[Key, MutableList[Value]]) = {
    var mapStr: String = ""
    result.foreach {
      case (key, list) => {
        mapStr += "\n\n>>>>>> key=" + key
        list.foreach {
          case (value) => mapStr += ", value=" + value
        }
      }
    }
    mapStr += "\n"
    log(mapStr, LogMessageType.CalculactionFinished)
  }

  def printLMap(result: Map[Key, List[Value]]) = {
    var mapStr: String = ""
    result.foreach {
      case (key, list) => {
        mapStr += "\n\n>>>>>> key=" + key
        list.foreach {
          case (value) => mapStr += ", value=" + value
        }
      }
    }
    mapStr += "\n"
    log(mapStr, LogMessageType.CalculactionFinished)
  }
}