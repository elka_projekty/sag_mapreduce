package pl.edu.pw.elka.sag.mapreduce.interfaces;


/*
 * Domyslne uzycie w agencie rozdzielajacym pliki:
 * in.setFileName('test.json')
 * while( in.hasNext() ){
 * 		MapActor ! in.getNextEntity()
 * 	}
 * 
 * 	gdzie in to obiekt implementujacy ten interfejs
 * 
 * */

public interface JEntityReader<InputType> {
	
	/*
	 * ustawienie pliku do odczytywanie
	 */
	public void setFileName(String fileName);
	
	/*
	 * Ma dzialac tak jak przy odczycie z pliku, czyli informowac, ze z pliku mozna jeszcze przeczytac cos
	 * */
	public boolean hasNext();
	
	/*
	 * Zwraca pojedynczy obiekt typu InputType 
	 * */	
	public InputType getNextEntity();
	
}
