package pl.edu.pw.elka.sag.mapreduce.job

import java.lang.reflect.Constructor
import java.util

import com.typesafe.config.ConfigFactory
import pl.edu.pw.elka.sag.mapreduce.config.JobConfiguration
import pl.edu.pw.elka.sag.mapreduce.actors._
import akka.actor._
import pl.edu.pw.elka.sag.mapreduce.interfaces._
import pl.edu.pw.elka.sag.mapreduce.reflection.MapReduceClassLoader
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import scala.collection.immutable.Stream.cons

class Job[InputType, Key, Value, OutputType](val jobConfiguration: JobConfiguration, val jobName: String) {

  def run: Unit = {
    //start master
    val cl = new MapReduceClassLoader(jobConfiguration.jarToLoad)
    val jMapperConstructor = cl.loadJMapperConstructor(jobConfiguration.mapperClassName).asInstanceOf[Constructor[JMapper[InputType, Key, Value]]]
    val jReducerConstructor = cl.loadJReducerConstructor(jobConfiguration.reducerClassName).asInstanceOf[Constructor[JReducer[Key, Value, OutputType]]]
    val jEntityReaderConstructor = cl.loadJReducerConstructor(jobConfiguration.entityReader).asInstanceOf[Constructor[JEntityReader[InputType]]]

    val config = ConfigFactory.parseString("akka.cluster.roles = [frontend]").
      withFallback(ConfigFactory.load())

    val system = ActorSystem("MapReduceSystem", config)

    val supervisor = system.actorOf(Props(
      new MapReduceMaster[InputType, Key, Value, OutputType](jobConfiguration.getFilesList(), jMapperConstructor, jReducerConstructor, jEntityReaderConstructor)),
      name = "supervisor")
    try{
      supervisor ! Calculate
    }catch{
      case _ : Exception => println("BLAD W MAPREDUCE")
    }
  }
}