package pl.edu.elka.pw.sag.mapreduce.tests.actors

import java.lang.reflect.Constructor

import akka.actor._
import akka.event.Logging
import akka.routing.RoundRobinRouter
import mapreduce.actors.MapReduceLogger
import mapreduce.actors.MapReduceLogger.LogMessageType
import mapreduce.helpers.MapReduceLogging
import pl.edu.pw.elka.sag.mapreduce.actors.MapWorker.ProcessLine
import pl.edu.pw.elka.sag.mapreduce.interfaces.JEntityReader
import pl.edu.pw.elka.sag.mapreduce.actors.FileName
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import pl.edu.elka.pw.sag.mapreduce.tests.Process
import pl.edu.elka.pw.sag.mapreduce.tests._

//testowy agent do czytania i plikow

class FileReaderTestActor[InputType](entityClassConstructor: Constructor[JEntityReader[InputType]]) extends Actor {

  val jEntityReaderInstance = entityClassConstructor.newInstance().asInstanceOf[JEntityReader[InputType]]

  def receive = {
    case Process(file) => {
      jEntityReaderInstance.setFileName(file)
      while (jEntityReaderInstance.hasNext())
        println(jEntityReaderInstance.getNextEntity)
      sender ! TestSupervisor.EndOfEntityTest(true)
    }
  }
}
