package pl.edu.elka.pw.sag.mapreduce.tests

import akka.actor._
import java.io.File
import java.lang.reflect.Constructor
import pl.edu.pw.elka.sag.mapreduce.interfaces._

object TestSupervisor {

  case class StartTest()

  case class StartEnityTest()
  case class StartMapTest()
  case class StartReduceTest()

  case class EndOfEntityTest(var result: Boolean)
  case class EndOfMapTest(var result: Boolean)
  case class EndOfReduceTest(var result: Boolean)
}

class TestSupervisor[InputType, Key, Value, OutputType](var fileList: List[File],
                                                        var mapClassConstructor: Constructor[JMapper[InputType, Key, Value]],
                                                        var reduceClassConstructor: Constructor[JReducer[Key, Value, OutputType]],
                                                        var entityClassConstructor: Constructor[JEntityReader[InputType]]) extends Actor {

  val entityReaderTestMaster = context.actorOf(
    Props(new EntityReaderTestMaster[InputType](entityClassConstructor, fileList)))

  var mapTestMaster: MapTestMaster[InputType, Key, Value] = null
  var reduceTestMaster: ReduceTestMaster[Key, Value, OutputType] = null

  def receive = {

    case TestSupervisor.StartTest() => entityReaderTestMaster ! TestSupervisor.StartEnityTest
    case TestSupervisor.EndOfEntityTest(res) =>
      if (res)
        entityReaderTestMaster ! TestSupervisor.StartMapTest
      else {
        println("KONIEC TESTOW. ERROR")
      }
    case TestSupervisor.EndOfMapTest(res)    => entityReaderTestMaster ! TestSupervisor.StartReduceTest
    case TestSupervisor.EndOfReduceTest(res) => println("KONIEC TESTOW")
  }
}