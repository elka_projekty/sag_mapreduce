package pl.edu.pw.elka.sag.mapreduce.main

import java.lang.reflect.Constructor

import akka.actor.{Props, _}
import com.typesafe.config.ConfigFactory
import mapreduce.actors.MapReduceLogger
import pl.edu.pw.elka.sag.mapreduce.actors.ReduceWorker
import pl.edu.pw.elka.sag.mapreduce.interfaces.JReducer
import pl.edu.pw.elka.sag.mapreduce.reflection.MapReduceClassLoader

object MainBackEnd {
  def main(args: Array[String]): Unit = {
    // Override the configuration of the port when specified as program argument
    val port = if (args.isEmpty) "0" else args(0)
    val config = ConfigFactory.parseString(s"akka.remote.netty.tcp.port=$port").
      withFallback(ConfigFactory.parseString("akka.cluster.roles = [backend]")).
      withFallback(ConfigFactory.load())


    val cl = new MapReduceClassLoader("resources/jars/jartoload.jar")
    val jReducerConstructor = cl.loadJReducerConstructor("pl.edu.pw.elka.sag.mapreduce.example.JSONReducerExample").asInstanceOf[Constructor[JReducer[String, Int, Int]]]

    val system = ActorSystem("MapReduceSystem", config)

    val loggerActor = system.actorOf(Props[MapReduceLogger], name = "MapReduceLogger")

    system.actorOf(Props(new ReduceWorker(loggerActor, jReducerConstructor)), name = "backend")
  }
}
