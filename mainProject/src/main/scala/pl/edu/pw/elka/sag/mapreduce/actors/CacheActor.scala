package mapreduce.actors

import akka.actor.{Actor, ActorRef}
import mapreduce.actors.CacheActor.{NewReducers, MapToCache, MapToShuffleSent}
import mapreduce.actors.MapReduceLogger.LogMessageType
import mapreduce.helpers.MapReduceLogging
import pl.edu.pw.elka.sag.mapreduce.actors.ReduceWorker.MapToReduce

import scala.collection.mutable
import scala.util.Random

object CacheActor {
  case class MapToCache[Key, Value](map: Map[Key, Value])
  case object MapToShuffleSent
  case class NewReducers(reducers: IndexedSeq[ActorRef])
}

class CacheActor[Key, Value](var loggerActor: ActorRef) extends Actor with MapReduceLogging {
  var mapToShuffleSentCounter = 0
  var mapsReceivedCounter = 0
  val mapWithAllValues: mutable.Map[Key, List[Value]] = mutable.Map[Key, List[Value]]()
  var reducers = IndexedSeq.empty[ActorRef]

  def addToMap(map: Map[Key, Value]) = {
    for (key <- map.keys) {
      val listToAdd: List[Value] = {
        if (mapWithAllValues.get(key).isDefined)
          mapWithAllValues(key)
        else
          List[Value]()
      }
      mapWithAllValues(key) = listToAdd :+ map(key)
    }
  }

  def mapsToSend():List[Map[Key, List[Value]]] = {
    mapWithAllValues.map(mapElem => Map[Key, List[Value]](mapElem._1 -> mapElem._2)).toList
  }

  override def receive: Receive = {
    case NewReducers(rs) =>
      reducers = rs
    case MapToCache(map) =>
      log(s"Got map to cache: $map", LogMessageType.GotMessage)
      addToMap(map.asInstanceOf[Map[Key, Value]])
      mapsReceivedCounter += 1
      if (mapsReceivedCounter == mapToShuffleSentCounter) {
        log(s"ALL MAPS RECEIVED $mapWithAllValues", LogMessageType.GotMessage)
        for (mapToBeReduced <- mapsToSend()) {
          reducers(Random.nextInt(reducers.length)) ! MapToReduce(mapToBeReduced)
        }
      }
    case MapToShuffleSent =>
      mapToShuffleSentCounter += 1
      log("shuffle sent counter increased to value: mapToShuffleSentCounter", LogMessageType.GotMessage)
  }
}
