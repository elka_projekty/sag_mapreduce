package mapreduce.helpers

import akka.actor.ActorRef
import mapreduce.actors.MapReduceLogger.LogMessage
import mapreduce.actors.MapReduceLogger.LogMessageType.LogMessageType

trait MapReduceLogging {
  var loggerActor:ActorRef
  def log(whatHappened: String, messageType:LogMessageType) {
    println(loggerActor)
    loggerActor ! LogMessage(this.getClass.toString, whatHappened, messageType)
  }
}
