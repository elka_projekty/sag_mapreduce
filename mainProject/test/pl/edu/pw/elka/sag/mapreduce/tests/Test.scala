package pl.edu.pw.elka.sag.mapreduce.tests

import pl.edu.pw.elka.sag.mapreduce.job.Job
import pl.edu.pw.elka.sag.mapreduce.config.JobConfiguration
import pl.edu.pw.elka.sag.mapreduce.interfaces._

object Test {

  def main(args: Array[String]) {
    //basic use case
    val jobConf = new JobConfiguration("".getClass, "".getClass, "/home/adminuser/", "", "".getClass, "".getClass)
    val job = new Job(jobConf, "test")
    job.run
  }
}


